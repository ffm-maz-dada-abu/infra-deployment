resource "kubernetes_ingress_v1" "example_ingress" {
  metadata {
    name = "ffm-ingress"
    annotations = {
        "kubernetes.io/ingress.class" = "alb"
        "alb.ingress.kubernetes.io/scheme" = "internet-facing"
        "alb.ingress.kubernetes.io/listen-ports" = "[{\"HTTP\": 80}]"
    }
  }

  spec {
    default_backend {
      service {
        name = "frontend"
        port {
          number = 3000
        }
      }
    }

    rule {
      http {
        path {
          path = "/api/v1/field-force/auth"
          path_type = "Prefix"
          backend {
            service {
              name = "auth"
              port {
                number = 8000
              }
            }
          }
        }
        path {
          path = "/api/v1/field-force/users"
          path_type = "Prefix"
          backend {
            service {
              name = "user"
              port {
                number = 3000
              }
            }
          }
        }
        path {
          path = "/"
          path_type = "Prefix"
          backend {
            service {
              name = "frontend"
              port {
                number = 3000
              }
            }
          }
        }
      }
    }
  }
}